Angular HttpClient
=================
Overview
---------------
The Angular HttpClient mechanism enables developers to call REST-like APIs.
According to the guide it: offers a simplified client HTTP API for Angular
applications that rests on the XMLHttpRequest interface exposed by browsers.
Additional benefits of HttpClient include testability features, typed request
and response objects, request and response interception, Observable apis, and
streamlined error handling

```shell
chromium-browser https://angular.io/guide/http

# install
docker run -it -h nghttp --name nghttp centos
yum install -y epel-release
yum install -y wget git-core which
curl -sL https://raw.githubusercontent.com/creationix/nvm/v0.33.11/install.sh |bash -
source ~/.bashrc
export CI=1
nvm --version
nvm install 10.15
npm install -g @angular/cli
git clone https://gitlab.com/harkwell/angular-poc.git /tmp/nghttp


# build and serve
ng --version # 7.2.3
node --version # v10.15.0
cd /tmp/nghttp/angular/
ng new --skip-git --verbose --routing=false --style=css \
   --directory=./build httpclient
\cp -r http-client/* build/ && cd build/
npm install
ng serve --host 0.0.0.0

# connect
IPADDR=$(docker inspect nghttp |grep IPAddress |tail -1 |cut -d\" -f4)
chromium-browser http://$IPADDR:4200

# clean-up
docker rm nghttp
```
