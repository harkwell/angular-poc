import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { HelloComponent } from 'src/app/hello/hello.component';

@NgModule({
    imports:
    [
        BrowserModule,
        // order matters, HttpClientModule should come after BrowserModule
        HttpClientModule
    ],
    declarations:
    [
        HelloComponent
    ],
    providers: [],
    bootstrap: [ HelloComponent ]
})
export class AppModule { }
