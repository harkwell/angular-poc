import { Component } from '@angular/core';
import { Greeting, HelloService } from './hello.service';

@Component({
    selector: 'app-root',
    templateUrl: './hello.component.html',
    providers: [ HelloService ],
    styles: ['.error {color: red;}']
})
export class HelloComponent
{
    error: any;
    headers: string[];
    greetings: Greeting[];

    constructor(private helloService: HelloService) {}

    clear()
    {
        this.greetings = undefined;
        this.error = undefined;
        this.headers = undefined;
    }

    showGreetings()
    {
        this.helloService.getGreetings().subscribe(
            (data: Greeting[]) => this.greetings = data,
            () => console.log("retrieved greetings")
        );
    }
}
