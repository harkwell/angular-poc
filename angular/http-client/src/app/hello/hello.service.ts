import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

export interface Greeting
{
    name: string;
    message: string;
}
@Injectable()
export class HelloService
{
    url = 'assets/list.json';
    // url = 'src/app/hello/assets/list.json';

    constructor(private http: HttpClient)
    {
        console.log("constructing HelloService");
    }

    getList()
    {
        return(this.http.get(this.url));
    }

    getGreetings(): Observable<Greeting[]>
    {
        return(this.http.get<Greeting[]>(this.url));
    }
}
