Angular CLI
=================
Overview
---------------
An Angular Command Line Interface for generating projects and serving them.

```shell
chromium-browser https://cli.angular.io/

# install nvm
docker run -it -h ng-cli --name ng-cli centos
yum install -y epel-release
curl -sL https://raw.githubusercontent.com/creationix/nvm/v0.33.11/install.sh |bash -
source ~/.bashrc
nvm --version

# install node and angular cli
nvm install 8.9
node --version
npm install -g @angular/cli # installs under $NVM_BIN/../lib/node_modules/

# create an arbitrary project, serve and connect
yum install -y git-core
git config --global user.email "kevin@khallware.com"
git config --global user.name "Kevin D.Hall"
ng new hello-world-app
cd hello-world-app/
sed -i -e 's#localhost#0.0.0.0#g' node_modules/@angular-devkit/build-angular/src/dev-server/schema.json
git log # notice that it has checked in all of the generated code
ng serve

IPADDR=$(docker inspect ng-cli |grep IPAddress |tail -1 |cut -d\" -f4)
chromium-browser http://$IPADDR:4200/
```
