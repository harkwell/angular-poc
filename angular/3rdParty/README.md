Third Party Tools
=================
Overview
---------------
Here are some useful third party Angular tools.

```shell
# compodoc - generate browseable code documents
chromium-browser https://compodoc.app/

# install nvm
docker run -it -h ng-tools --name ng-tools centos
yum install -y epel-release
yum install -y git-core
curl -sL https://raw.githubusercontent.com/creationix/nvm/v0.33.11/install.sh |bash -
source ~/.bashrc
nvm --version

# install node and compodoc
nvm install 8.9
node --version
npm install -g @compodoc/compodoc # installs under $NVM_BIN/../lib/node_modules/

# checkout an angular based code repo
git clone https://github.com/primefaces/primeng.git && cd primeng

# generate docs
compodoc -p src/tsconfig.app.json

# retrieve docs and browse
docker cp ng-tools:/primeng/documentation /tmp/mydocs
chromium-browser file:///tmp/mydocs/index.html
```

* [Faker](faker) - The faker library that generates random json data.
