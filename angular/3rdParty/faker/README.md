Faker.js
=================
Overview
---------------
This library is useful for generating random json data.

```shell
# install
docker run -it -h ngfaker --name ngfaker centos
yum install -y epel-release
curl -sL https://raw.githubusercontent.com/creationix/nvm/v0.33.11/install.sh |bash -
source ~/.bashrc
nvm --version
yum install -y git-core which
nvm install 10.15
npm install -g yarn
yarn global add @angular/cli
git clone https://gitlab.com/harkwell/angular-poc.git /tmp/ngfaker

# build
ng --version # 7.3.7
node --version # v10.15.3
cd /tmp/ngfaker/angular/
ng new --minimal --skip-git --skip-install --skip-tests --verbose --routing=false --style=css --directory=./build faker
cp -r 3rdParty/faker/* build/ && cd build/
yarn install
yarn add faker

# serve
export CI=$(false)
ng serve --poll 2000 --host 0.0.0.0 --disableHostCheck

# test
IPADDR=$(docker inspect ngfaker |grep IPAddress |tail -1 |cut -d\" -f4)
printf "GET /index.html HTTP/1.1\nHost: $IPADDR\n\n" |nc $IPADDR 4200
chromium-browser http://$IPADDR:4200/

# clean-up
exit
docker rm ngfaker
```
