import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { MockPocService } from './mockpoc.service';
import { PocComponent } from './poc.component';
import { PocService } from './poc.service';

@NgModule({
    declarations: [
        PocComponent,
    ],
    imports: [
        BrowserModule,
	HttpClientModule,
    ],
    providers: [
        PocService,
	{ provide: HTTP_INTERCEPTORS, useClass: MockPocService, multi: true }
    ],
    bootstrap: [ PocComponent ]
})
export class AppModule { }
