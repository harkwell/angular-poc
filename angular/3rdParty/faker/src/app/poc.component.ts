import { PocService  } from './poc.service';
import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'app-root',
    template: `
        <div style="text-align:center">
           <div>{{ description }}</div>
           <div *ngIf="title; let title; else loading1">
              <div>Title: {{ title?.title }}</div>
           </div>
           <ng-template #loading1>Loading Title...</ng-template>
        </div>

        <ul *ngIf="list; let list; else loading2">
            <li *ngFor="let item of list?.data">
               <span [textContent]="item"></span>
               <!-- span>{{ item }}</span -->
	    </li>
        </ul>
        <ng-template #loading2>Loading List...</ng-template>
    `,
    styles: []
})
export class PocComponent implements OnInit
{
	description = "In Memory Data Service HTTP Mocking and Faker"
	title = undefined
	list = undefined

	constructor(private pocService: PocService) {}

	ngOnInit()
	{
		console.log("ngOnInit() called...")
		console.log("ping...")

		this.pocService.ping().subscribe(
			obj => {
				console.log(obj)
			},
			error => this.handleError(error)
		)
		this.pocService.getTitle().subscribe(
			t => {
				this.title = t
			 	console.log("title found: ")
			 	console.log(t)
			},
			error => this.handleError(error)
		)

		this.pocService.getList().subscribe(
			l => {
				this.list = l
			 	console.log("list found: ")
			 	console.log(l)
			},
			error => this.handleError(error)
		)
	}

	handleError(error)
	{
		console.log("An error has occurred.")
		console.log(error)
	}
}
