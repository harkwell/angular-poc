import {
	HttpInterceptor,
	HttpResponse,
	HttpRequest,
	HttpHandler,
	HttpEvent
} from '@angular/common/http';

import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import * as faker from 'faker/locale/en_US';

@Injectable()
export class MockPocService implements HttpInterceptor
{
	private title = { 'title' : 'sometitle' }
	private list = { 'data' : ["foo", "bar", "baz", "doh", "blah", "meh"] }

	intercept(req: HttpRequest<any>): Observable<HttpEvent<any>>
	{
		var retval
		console.log("request url: "+req.url)

		if (req.url.endsWith("things")) {
			console.log("MockPocService handling \"things\"")
			// retval = this.makeResponse(this.list)
			retval = this.makeResponse(this.makeFakeList())
		}
		else {
			console.log("MockPocService handling \"title\"")
			// retval = this.makeResponse(this.title)
			retval = this.makeResponse(this.makeFakeTitle())
		}
		return(retval)
	}

	makeResponse(obj: any): Observable<HttpEvent<any>>
	{
		var retval = new HttpResponse({ status: 200, body: obj })
		return(new Observable(response => {
			response.next(retval);
			response.complete();
		}))
	}

	makeFakeTitle(): any
	{
		var retval = { 'title' : '' }
		retval.title = faker.lorem.sentence()
		return(retval)
	}

	makeFakeList(): any
	{
		var retval = { 'data' : [] }
		var len = faker.random.number(50)

		for (var idx=0; idx<len; idx++) {
			retval.data[idx] = faker.lorem.sentence()
		}
		return(retval)
	}
}
