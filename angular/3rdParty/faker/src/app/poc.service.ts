import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError } from 'rxjs/operators';

@Injectable()
export class PocService
{
	uri1 = "/apis/v1.0/things/1/titles/1"
	uri2 = "/apis/v1.0/things"

	constructor(private http: HttpClient)
	{
		console.log("constructing PocService");
	}

	getTitle(): Observable<{}>
	{
		let failResponse = { 'title' : 'fail' }
		console.log("calling getTitle() from PocService");
		return(this.http.get<{}>(this.uri1)
			.pipe(catchError(
				this.handleFail<{}>("getTitle()",
					failResponse))))
	}

	getList(): Observable<{}>
	{
		let failResponse = { 'data' : ["fail"] }
		console.log("calling getList() from PocService");
		return(this.http.get<{}>(this.uri2)
			.pipe(catchError(
				this.handleFail<{}>("getList()",
					failResponse))))
	}

	ping(): Observable<{}>
	{
		return(of({ ping: 'pong' }))
	}

	private handleFail<T> (operation = 'operation', retval?: T)
	{
		return((error: any): Observable<T> => {
			console.error(error)
			return(of(retval as T))
		})
	}
}
