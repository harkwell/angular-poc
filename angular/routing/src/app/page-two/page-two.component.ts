import { Component, OnInit } from '@angular/core';

@Component({
	selector    : 'app-page-two',
	templateUrl : './page-two.component.html',
	styleUrls   : [ './page-two.component.css' ]
})

export class PageTwoComponent implements OnInit
{
	constructor()
	{
		console.log("this is the page two component ctor");
	}

	ngOnInit()
	{
		console.log("this is the page two init routine");
	}
}
