import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { Router, RouterModule, Routes } from '@angular/router';
import { AppComponent } from './app.component';
import { PageOneComponent } from './page-one/page-one.component';
import { PageTwoComponent } from './page-two/page-two.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';

const pocRoutes : Routes = [
	/** Order matters... most specific (top) to least specific (bottom) **/
	{
		path       : 'page-one',
		component  : PageOneComponent
	},
	{
		path       : 'page-two',
		component  : PageTwoComponent
	},
	{
		path       : '',              /** The default path **/
		redirectTo : '/page-one',
		pathMatch  : 'full'
	},
	{
		path       : '**',            /** The catch-all path **/
		component  : PageNotFoundComponent
	}
];

@NgModule({
	declarations : [
		AppComponent,
		PageOneComponent,
		PageTwoComponent,
		PageNotFoundComponent
	],
	imports      : [
		BrowserModule,
		RouterModule.forRoot(
			pocRoutes,
			{
				enableTracing : true
			}
		)
	],
	providers    : [ ],
	bootstrap    : [ AppComponent ]
})
export class AppModule
{
	constructor(router: Router)
	{
		const replacer = (key, value) => (typeof value === 'function')
			? value.name
			: value;
		console.log('Routes: ',
			JSON.stringify(router.config, replacer, 2));
	}
}
