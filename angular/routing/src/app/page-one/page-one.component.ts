import { Component, OnInit } from '@angular/core';

@Component({
	selector    : 'app-page-one',
	templateUrl : './page-one.component.html',
	styleUrls   : [ './page-one.component.css' ]
})

export class PageOneComponent implements OnInit
{
	constructor()
	{
		console.log("this is the page one component ctor");
	}

	ngOnInit()
	{
		console.log("this is the page one init routine");
	}
}
