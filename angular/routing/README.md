Angular Routing
=================
Overview
---------------
The Angular Router enables navigation from one view to the next as users
perform application tasks.  The example here contains the very minimal of
what one would expect to leverage Angular Routing in a single page
application.

```shell
chromium-browser https://angular.io/guide/router

# install
docker run -it -h ngrouting --name ngrouting centos
yum install -y epel-release
curl -sL https://raw.githubusercontent.com/creationix/nvm/v0.33.11/install.sh |bash -
source ~/.bashrc
nvm --version
yum install -y git-core which
nvm install 8.9
npm install -g @angular/cli
git clone https://gitlab.com/harkwell/angular-poc.git /tmp/ngrouting

# build
ng --version # 7.1.4
node --version # 8.9.4
cd /tmp/ngrouting/angular/
ng new --minimal --skip-git --skip-install --skip-tests --verbose --routing=false --style=css --directory=./build routing
cp -r routing/* build/ && cd build/
#npm install @angular-devkit/build-angular
npm install

# serve
export CI=$(false)
ng serve --host 0.0.0.0

# test
IPADDR=$(docker inspect ngrouting |grep IPAddress |tail -1 |cut -d\" -f4)
chromium-browser http://$IPADDR:4200/

# clean-up
docker rm ngrouting
```
