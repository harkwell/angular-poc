import { TestBed, getTestBed, async } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from  '@angular/common/http/testing';
import { HelloService } from './hello.service';

describe('HelloService', () =>
{
    let ijector: TestBed;
    let service: HelloService;
    let httpMock: HttpTestingController;

    beforeEach(async(() =>
    {
        TestBed.configureTestingModule({
            imports: [ HttpClientTestingModule ],
            providers: [ HelloService ]
        });
        ijector = getTestBed();
        service = ijector.get(HelloService);
        httpMock = ijector.get(HttpTestingController);
    }));
    afterEach(() =>
    {
        httpMock.verify();
    });

    it('test 1: return type should be Observable<Greeting[]>', () =>
    {
        const outJson = require('./assets/list.json');

        service.getGreetings().subscribe(rslt =>
        {
            expect(outJson.length).toBe(2);
            expect(outJson).toEqual(outJson);
        });
        const req  = httpMock.expectOne("assets/list.json");
        expect(req.request.method).toBe("GET");
        req.flush(outJson);
    });
});
