Angular Testing (HttpClient)
=================
Overview
---------------
The Angular HttpClient testing facility enables developers to unit test
their applications REST API calls by mocking the request and response.

```shell
chromium-browser https://angular.io/api/common/http/testing/HttpTestingController

docker run -it -h nghttptesting --name nghttptesting --privileged=true centos
yum install -y epel-release
yum install -y wget git-core which
wget -c 'https://dl.google.com/linux/direct/google-chrome-stable_current_x86_64.rpm' -O /tmp/chrome.rpm
yum localinstall -y /tmp/chrome.rpm
\rm /tmp/chrome.rpm
useradd angular
su - angular
curl -sL https://raw.githubusercontent.com/creationix/nvm/v0.33.11/install.sh |bash -
source ~/.bashrc
export CI=1
nvm --version
nvm install 10.15
npm install -g @angular/cli
git clone https://gitlab.com/harkwell/angular-poc.git /tmp/nghttptesting

# build
ng --version # 7.2.3
node --version # v10.15.0
cd /tmp/nghttptesting/angular/
ng new --skip-git --verbose --routing=false --style=css --directory=./build testing
\cp -r http-client/* build/ && \cp -r testing/http-client/* build/ && cd build/
npm install

# test
ng test --browsers ChromeHeadless

# connect
IPADDR=$(docker inspect nghttptesting |grep IPAddress |tail -1 |cut -d\" -f4)
chromium-browser http://$IPADDR:4200
```
