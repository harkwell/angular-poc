import { TestBed, async } from '@angular/core/testing';
import { AppComponent } from './app.component';

describe('AppComponent', () =>
{
	beforeEach(async(() =>
	{
		TestBed.configureTestingModule({
			declarations: [ AppComponent ],
		}).compileComponents();
	}));

	it('ngtesting test 1', () =>
	{
		expect(true).toBeTruthy();
	});
	it('ngtesting test 2', () =>
	{
		const fixture = TestBed.createComponent(AppComponent);
		const app = fixture.debugElement.componentInstance;
		expect(app).toBeTruthy();
	});
});
