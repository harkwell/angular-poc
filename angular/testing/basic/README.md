Angular Testing
=================
Overview
---------------
The Angular testing facility enables developers to unit and integration test
their application.

```shell
chromium-browser https://angular.io/guide/testing

# install
docker run -it -h ngtesting --name ngtesting --privileged=true centos
yum install -y epel-release
yum install -y wget git-core which
wget -c 'https://dl.google.com/linux/direct/google-chrome-stable_current_x86_64.rpm' -O /tmp/chrome.rpm
yum localinstall -y /tmp/chrome.rpm
rm /tmp/chrome.rpm
useradd angular
su - angular
curl -sL https://raw.githubusercontent.com/creationix/nvm/v0.33.11/install.sh |bash -
source ~/.bashrc
export CI=1
nvm --version
nvm install 8.9
npm install -g @angular/cli
git clone https://gitlab.com/harkwell/angular-poc.git /tmp/ngtesting


# build
ng --version # 7.2.2
node --version # 8.9.4
cd /tmp/ngtesting/angular/testing
ng new --skip-git --verbose --routing=false --style=css --directory=./build testing
cp -r basic/* build/ && cd build/
npm install

# inspect
cat src/karma.conf.js # notice: jasmine, karma, browsers
cat src/test.ts       # notice: getTestBed, filename regex ".spec.ts$"

# test
ng test --browsers ChromeHeadless

# clean-up
docker rm ngtesting
```
