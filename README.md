Khallware (Angular Proofs-of-Concept)
=================
Overview
---------------
Here are some simple, self-contained proofs-of-concept written in AngularJs and
Angular (typescript).

Topics include:

* [Basics](basics) - simple angular javascript framework features

```shell
chromium-browser http://angularjs.org/ http://docs.angularjs.org/api/
```
