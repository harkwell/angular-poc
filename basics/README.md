AngularJs Basics (Fundamentals)
=================
Overview
---------------
AngularJs topics here include:

* [Data Binding](databinding) - Bind Data from some model source
* [Module](modules) - Module is a packaging container for: routes, controllers, factories, services, directives and filters
* [Route](routes) - Routes are parts of the URL that dictate what views are used
* [View](views) - Views are the pieces that are rendered for the user
* [Directive](directives) - Directives enhance view html functionality
* [Filter](filters) - Filters
* [Controller](controllers) - Controllers process flow using factories and services on the view
* [Factory](factories) - Factories
* [Service](services) - Services are singletons
