angular.module('charactersApp', []);

(function ()
{
	var ctrl = function ($scope)
	{
		$scope.characters = [
			{
				name : 'Jack',
				what : 'Main character bests witch.'
			},
			{
				name : 'Will',
				what : 'Brother of Jack gets bested by witch'
			},
			{
				name : 'Tom',
				what : 'Brother of Jack gets bested by witch'
			},
			{
				name : 'Kings Girl',
				what : 'Reward to whomever can best the witch'
			},
			{
				name : 'Old Man',
				what : 'Gift giver when shown kindness'
			},
			{
				name : 'Witch',
				what : 'Wealthy and competitive evil person'
			},
			{
				name : 'Hardy Hardhead',
				what : 'Impenetrable noggin'
			},
			{
				name : 'Eatwell',
				what : 'Bottomless eating machine'
			},
			{
				name : 'Drinkwell',
				what : 'Bottomless drinking machine'
			},
			{
				name : 'Runwell',
				what : 'Tireless, extremely fast runner'
			},
			{
				name : 'Harkwell',
				what : 'Hears everything'
			},
			{
				name : 'Seewell',
				what : 'Perfectly supernatural eyesight'
			},
			{
				name : 'Shootwell',
				what : 'Able to place a round at any distance'
			}
		];
	};
	angular.module('charactersApp').controller('charactersController',ctrl);
}());
