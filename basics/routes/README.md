AngularJs Routes
=================
Overview
---------------
This proof-of-concept for routing uses the example of the story of Goldilocks
and the Three Bears.  The main page welcomes the visitor to the story, the
second page has Goldilocks at the house, page three is where she eats the
porridge, page four is the chair and lastly the fifth page is the bed.


```shell
sudo bash -o vi
docker run -it -h ng-poc --name ng-poc -v /tmp/ng-poc:/tmp centos
yum install -y epel-release
yum install -y git-core nginx
cd /tmp && git clone https://gitlab.com/harkwell/angular-poc.git
ln -s /tmp/angular-poc/basics/routes/ /usr/share/nginx/html/ng-poc
nginx
#chromium-browser --allow-file-access-from-files index.html
IPADDR=$(docker inspect ng-poc |grep IPAddress |tail -1 |cut -d\" -f4)
chromium-browser http://$IPADDR/ng-poc/index.html

# clean up
docker rm -f ng-poc && rm -rf /tmp/ng-poc
```
