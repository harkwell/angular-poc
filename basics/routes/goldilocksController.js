angular.module('goldilocksApp', ['ngRoute']);

(function ()
{
	var routes = function ($routeProvider)
	{
		$routeProvider.when('/', {
			templateUrl : 'welcome.html.part'
		})
		.when('/house', {
			templateUrl : 'house.html.part'
		})
		.when('/porridge', {
			templateUrl : 'porridge.html.part'
		})
		.when('/chair', {
			templateUrl : 'chair.html.part'
		})
		.when('/bed', {
			templateUrl : 'bed.html.part'
		})
		.otherwise({ redirectTo : '/' });
	};
	angular.module('goldilocksApp').config(routes);
}());
